﻿create table Position(
Id int identity primary key,
name varchar(96) not null);
create table HireOrder(
Id int identity primary key);
create table FireOrder(
Id int identity primary key);
create table Employee(
Id int identity primary key,
LastName varchar(64) not null,
FirstName varchar(64) not null,
BirthDay datetime not null,
PositionId int,
HireOrderId int,
FireOrderId int,
foreign key(PositionId) references Position (Id) on delete set null on update cascade,
foreign key(HireOrderId) references HireOrder (Id) on delete set null on update cascade,
foreign key(FireOrderId) references FireOrder (Id) on delete set null on update cascade,
Photo varbinary(max) not null);
create table ProgramUser (
Id int identity primary key,
Username varchar(32) not null,
CanWrite bit default 0);
insert into ProgramUser (Username, CanWrite, Password) values ('admin', 1, '12345');
insert into ProgramUser (Username, CanWrite, Password) values ('user', 0, '12345');