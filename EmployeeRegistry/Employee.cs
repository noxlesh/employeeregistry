﻿using System;
using System.Drawing;

namespace EmployeeRegistry
{
    public class Employee
    {
        public Employee()
        {

        }

        public Employee(string firstName, string lastName, DateTime birdhday, int positionId, int hireId, int fireId, Image photo)
        {
            FirstName = firstName;
            LastName = lastName;
            Birdhday = birdhday;
            PositionId = positionId;
            HireId = hireId;
            FireId = fireId;
            Photo = photo;
        }

        public const int MaxFirstName = 64;
        public const int MaxLastName = 64;

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Birdhday { get; set; }
        public int PositionId { get; set; }
        public int HireId { get; set; }
        public int FireId { get; set; }
        public Image Photo { get; set; }
    }
}
