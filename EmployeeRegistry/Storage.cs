﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;

namespace EmployeeRegistry
{
    public enum UserStatus
    {
        NotFound,
        ReadWrite,
        ReadOnly,
    }

    public static class Storage
    {
        public delegate void ErrorHandler(string errStr);
        public static event ErrorHandler OnDBError;
        private static SqlConnection conn;
        public const string datetimeFormat = "yyyy-MM-dd";

        public static void InitDbConnection()
        {
            conn = new SqlConnection(
                 new SqlConnectionStringBuilder
                 {
                     UserID = "sa",
                     Password = "ndf0rmny",
                     IntegratedSecurity = false,
                     InitialCatalog = "employee",
                     DataSource = "10.1.10.2",
                 }.ConnectionString);
        }

        private static void TryOpen()
        {
            try
            {
                conn.Open();
            }
            catch (SqlException ex)
            {
                OnDBError?.Invoke(ex.Message);
            }
        }

        public static UserStatus GetUser(string name, string passwd)
        {
            TryOpen();
            UserStatus userStatus = UserStatus.NotFound;
            SqlCommand command = conn.CreateCommand();
            command.CommandText = $"SELECT CanWrite FROM ProgramUser WHERE Username='{name}' AND Password='{passwd}'";
            SqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                if (reader.GetBoolean(reader.GetOrdinal("CanWrite")))
                    userStatus = UserStatus.ReadWrite;
                else
                    userStatus = UserStatus.ReadOnly;

            }
            reader.Close();
            conn.Close();
            return userStatus;
        }

        public static void AddPosition(Position p)
        {
            TryOpen();
            SqlCommand command = conn.CreateCommand();
            command.CommandText = $"INSERT INTO Position (name) VALUES ('{p.Name}')";
            command.ExecuteNonQuery();
            conn.Close();
        }

        public static Position GetPosition(int Id)
        {
            TryOpen();
            Position p = null;
            SqlCommand command = conn.CreateCommand();
            command.CommandText = $"SELECT name FROM Position WHERE Id={Id}";
            SqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                p = new Position(reader.GetString(reader.GetOrdinal("name")));

            }
            reader.Close();
            conn.Close();
            return p;
        }

        public static DataSet GetPositions()
        {
            TryOpen();
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand command = conn.CreateCommand();
            command.CommandText = @"SELECT * FROM Position";
            da.SelectCommand = command;
            da.Fill(ds);

            conn.Close();
            return ds;
        }

        public static void AddEmployee(Employee e)
        {

            SqlCommand command = conn.CreateCommand();
            byte[] photo = ImageToByte(e.Photo);
            int hireId = AddHireOrder();
            if (hireId == -1)
                return;
            command.CommandText = "INSERT INTO Employee (LastName, FirstName, BirthDay, PositionId, HireOrderId, Photo)" +
                " VALUES (@FirstName, @LastName, @BirthDay, @PositionId, @HireId, @FireId, @Photo)";
            command.Parameters.Add("@LastName", SqlDbType.NVarChar, 64).Value = e.LastName;
            command.Parameters.Add("@FirstName", SqlDbType.NVarChar, 64).Value = e.FirstName;
            command.Parameters.Add("@BirthDay", SqlDbType.DateTime).Value = e.Birdhday;
            command.Parameters.Add("@PositionId", SqlDbType.Int).Value = e.PositionId;
            command.Parameters.Add("@HireId", SqlDbType.Int).Value = hireId;
            command.Parameters.Add("@FireId", SqlDbType.Int).Value = 0;
            command.Parameters.Add("@Photo", SqlDbType.VarBinary, -1).Value = photo;
            TryOpen();
            command.ExecuteNonQuery();
            conn.Close();
        }

        public static void EditEmployee(Employee e)
        {
            SqlCommand command = conn.CreateCommand();
            byte[] photo = ImageToByte(e.Photo);
            command.CommandText = "UPDATE Employee SET LastName=@LastName, FirstName=@FirstName, " +
                "BirthDay=@BirthDay, PositionId=@PositionId, HireOrderId=@HireId, " +
                "FireOrderId=@FireId , Photo=@Photo WHERE Id=@Id;";
            command.Parameters.Add("@Id", SqlDbType.Int).Value = e.Id;
            command.Parameters.Add("@LastName", SqlDbType.NVarChar, 64).Value = e.LastName;
            command.Parameters.Add("@FirstName", SqlDbType.NVarChar, 64).Value = e.FirstName;
            command.Parameters.Add("@BirthDay", SqlDbType.DateTime).Value = e.Birdhday;
            command.Parameters.Add("@PositionId", SqlDbType.Int).Value = e.PositionId;
            command.Parameters.Add("@HireId", SqlDbType.Int).Value = e.HireId;
            if(e.FireId == 0)
                command.Parameters.Add("@FireId", SqlDbType.Int).Value = DBNull.Value;
            else
                command.Parameters.Add("@FireId", SqlDbType.Int).Value = e.FireId;
            command.Parameters.AddWithValue("@Photo", photo);
            TryOpen();
            command.ExecuteNonQuery();
            conn.Close();
        }

        public static Int32 AddHireOrder()
        {
            TryOpen();
            SqlCommand command = conn.CreateCommand();
            command.CommandText = "INSERT HireOrder DEFAULT VALUES;SELECT SCOPE_IDENTITY();";
            int hireId = Convert.ToInt32(command.ExecuteScalar());
            conn.Close();
            return hireId;
        }

        public static Int32 AddFireOrder()
        {
            TryOpen();
            SqlCommand command = conn.CreateCommand();
            command.CommandText = "INSERT FireOrder DEFAULT VALUES;SELECT SCOPE_IDENTITY();";
            int fireId = Convert.ToInt32(command.ExecuteScalar());
            conn.Close();
            return fireId;
        }

        public static void FireEmployee(int Id)
        {
            List<Employee> le = new List<Employee>();
            int fireId = AddFireOrder();
            SqlCommand command = conn.CreateCommand();
            command.CommandText = "UPDATE Employee SET FireOrderId=@FireId WHERE Id=@Id;";
            command.Parameters.Add("@Id", SqlDbType.Int).Value = Id;
            command.Parameters.Add("@FireId", SqlDbType.Int).Value = fireId;
            TryOpen();
            command.ExecuteNonQuery();
            conn.Close();
        }

        public static List<Employee> GetEmployees(int Id = 0)
        {
            TryOpen();
            List<Employee> le = new List<Employee>();
            SqlCommand command = conn.CreateCommand();
            if (Id > 0)
                command.CommandText = $"SELECT * FROM Employee WHERE Id={Id}";
            else
                command.CommandText = $"SELECT * FROM Employee";
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                Image photo = ByteToImage((byte[])reader["Photo"]);
                Employee e = new Employee
                {
                    Id = reader.GetInt32(reader.GetOrdinal("Id")),
                    FirstName = reader.GetString(reader.GetOrdinal("FirstName")),
                    LastName = reader.GetString(reader.GetOrdinal("LastName")),
                    Birdhday = reader.GetDateTime(reader.GetOrdinal("BirthDay")),
                    PositionId = reader.GetInt32(reader.GetOrdinal("PositionId")),
                    HireId = reader.GetInt32(reader.GetOrdinal("HireOrderId")),
                    FireId = !reader.IsDBNull(reader.GetOrdinal("FireOrderId")) ? reader.GetInt32(reader.GetOrdinal("FireOrderId")) : default(int),
                    Photo = photo
                };
                le.Add(e);
            }
            reader.Close();
            conn.Close();
            return le;
        }

        private static byte[] ImageToByte(Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
            byte[] arr = ms.ToArray();
            ms.Dispose();
            return arr;
        }

        private static Image ByteToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            ms.Dispose();
            return returnImage;
        }

    }
}