﻿using System;
using System.Windows.Forms;
using System.Data;

namespace EmployeeRegistry
{
    public partial class PositionListDialog : Form
    {
        public PositionListDialog()
        {
            InitializeComponent();
            FillListView(Storage.GetPositions());
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            Form addPosition = new AddPositionDialog();
            addPosition.ShowDialog(this);
        }

        public override void Refresh()
        {
            listViewPosition.Items.Clear();
            FillListView(Storage.GetPositions());
            base.Refresh();
        }

        private void FillListView(DataSet ds)
        {
            DataTable dt = ds.Tables[0];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                if(dr.RowState != DataRowState.Deleted)
                {
                    ListViewItem li = new ListViewItem(dr["Id"].ToString());
                    li.SubItems.Add(dr["name"].ToString());
                    listViewPosition.Items.Add(li);
                }
            }
        }

    }
}
