﻿using System;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace EmployeeRegistry
{
    public partial class EmployeeDialog : Form
    {
        private Image loadedImage;
        private int employeeId = 0;
        private int hireId = 0;
        private int fireId = 0;

        public EmployeeDialog()
        {
            InitializeComponent();
            FillComboBox(Storage.GetPositions());
        }

        public EmployeeDialog(Employee em)
        {
            InitializeComponent();
            FillComboBox(Storage.GetPositions());

            FillForm(em);
        }

        private void buttonPhoto_Click(object sender, EventArgs e)
        {
            OpenFileDialog fd = new OpenFileDialog();
            if (fd.ShowDialog() == DialogResult.OK)
            {
                loadedImage = Image.FromFile(fd.FileName, false);
                pictureBox.Image = ResizeImage(loadedImage, pictureBox.Width, pictureBox.Height);
            }
            
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonApply_Click(object sender, EventArgs e)
        {
            if(String.IsNullOrEmpty(textBoxFirstName.Text) && textBoxFirstName.Text.Length > Employee.MaxFirstName)
            {
                MessageBox.Show("Incorrect First Name field! Can't add.");
                return;
            }
            if (String.IsNullOrEmpty(textBoxLastName.Text) && textBoxLastName.Text.Length > Employee.MaxLastName)
            {
                MessageBox.Show("Incorrect Last Name field! Can't add.");
                return;
            }
            if (dateTimePickerBirthday.Value == DateTime.Now)
            {
                MessageBox.Show("Birthday can't be today! Can't add.");
                return;
            }
            if (pictureBox.Image == null)
            {
                MessageBox.Show("Photo wasn't inserted! Can't add.");
                return;
            }
            int positionId = Int32.Parse(comboBoxPosition.SelectedItem.ToString().Split(' ')[0]);
            Employee employee = new Employee(textBoxFirstName.Text, textBoxLastName.Text,
                dateTimePickerBirthday.Value, positionId, hireId, fireId, pictureBox.Image);
            if (employeeId > 0) // if this dialog is opened to edit an employee profile.
            {
                employee.Id = employeeId;
                Storage.EditEmployee(employee);
            }
            else
                Storage.AddEmployee(employee);
            Close();
        }

        /// <summary>
        /// Resizes the image to the specified width and height.
        /// </summary>
        /// <param name="image">The image to resize.</param>
        /// <param name="width">The width to resize to.</param>
        /// <param name="height">The height to resize to.</param>
        /// <returns>The resized image.</returns>
        private Image ResizeImage(Image image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage as Image;
        }

        private void FillForm(Employee e)
        {
            textBoxFirstName.Text = e.FirstName;
            textBoxLastName.Text = e.LastName;
            comboBoxPosition.SelectedIndex = comboBoxPosition.FindString($"{e.PositionId}");
            dateTimePickerBirthday.Value = e.Birdhday;
            employeeId = e.Id;
            hireId = e.HireId;
            fireId = e.FireId;
            pictureBox.Image = e.Photo; 
        }

        private void FillComboBox(DataSet ds)
        {
            DataTable dt = ds.Tables[0];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                if (dr.RowState != DataRowState.Deleted)
                {
                    string str = $"{dr["Id"].ToString()} {dr["name"].ToString()}";
                    comboBoxPosition.Items.Add(str);
                    comboBoxPosition.SelectedIndex = 0;
                }
            }
        }
    }
}
