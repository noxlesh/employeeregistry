﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmployeeRegistry
{
    public partial class AddPositionDialog : Form
    {
        public AddPositionDialog()
        {
            InitializeComponent();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Owner.Refresh();
            Close();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            if(String.IsNullOrEmpty(textBoxPosition.Text) && textBoxPosition.Text.Length > Position.MaxSize)
            {
                MessageBox.Show("Incorrect position field! Can't add.");
                return;
            }
            Position p = new Position(textBoxPosition.Text);
            Storage.AddPosition(p);
            Owner.Refresh();
            Close();
        }
    }
}
