﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace EmployeeRegistry
{
    public partial class MainForm : Form
    {
        private UserStatus userStatus = UserStatus.NotFound;

        public MainForm()
        {
            InitializeComponent();
            Storage.InitDbConnection();
            Storage.OnDBError += (e) => MessageBox.Show(e);
        }

        private void FillEmployeeList(List<Employee> le)
        {
            foreach (var e in le)
            {
                ListViewItem li = new ListViewItem(e.Id.ToString());
                li.SubItems.Add(e.FirstName);
                li.SubItems.Add(e.LastName);
                li.SubItems.Add(Storage.GetPosition(e.PositionId).Name);
                li.SubItems.Add(e.Birdhday.ToString(Storage.datetimeFormat));
                li.SubItems.Add(e.HireId.ToString());
                li.SubItems.Add(e.FireId.ToString());
                listViewEmployee.Items.Add(li);
            }
        }

        #region Form Event Handlers

        private void MainForm_Load(object sender, EventArgs e)
        {
            Hide();
            LoginDialog loginDialog = new LoginDialog();
            loginDialog.ShowDialog(this);
            this.userStatus = loginDialog.Status;
            
            if (userStatus == UserStatus.NotFound)
            {
                Close();
                return;
            }
            FillEmployeeList(Storage.GetEmployees());
            
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void positionListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PositionListDialog positionList = new PositionListDialog();
            positionList.ShowDialog();
        }

        

        private void employeeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form addEmployee = new EmployeeDialog();
            addEmployee.ShowDialog(this);
            listViewEmployee.Items.Clear();
            FillEmployeeList(Storage.GetEmployees());
        }

        private void positionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form addPosition = new AddPositionDialog();
            addPosition.ShowDialog(this);
        }

        private void listViewEmployee_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (listViewEmployee.FocusedItem.Bounds.Contains(e.Location) == true)
                {
                    contextMenuStrip.Show(Cursor.Position);
                }
            }
        }

        private void toolStripMenuItemEdit_Click(object sender, EventArgs e)
        {
            int i = listViewEmployee.SelectedIndices[0];
            int employeeId = Int32.Parse(listViewEmployee.Items[i].Text);
            Employee employee = Storage.GetEmployees(employeeId)[0];
            Form addEmployee = new EmployeeDialog(employee);
            addEmployee.ShowDialog(this);
        }

        private void toolStripMenuItemFire_Click(object sender, EventArgs e)
        {
            int i = listViewEmployee.SelectedIndices[0];
            int employeeId = Int32.Parse(listViewEmployee.Items[i].Text);
            Storage.FireEmployee(employeeId);
            listViewEmployee.Items.Clear();
            FillEmployeeList(Storage.GetEmployees());
        }

        #endregion
    }
}
