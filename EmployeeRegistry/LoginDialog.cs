﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace EmployeeRegistry
{
    public partial class LoginDialog : Form
    {
        public UserStatus Status { get; private set; }
        public LoginDialog()
        {
            InitializeComponent();
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            Status = Storage.GetUser(textBoxLogin.Text, textBoxPass.Text);
            if (Status == UserStatus.NotFound)
                MessageBox.Show("Login or password incorrect!");
            else
                Close();
        }
    }
}
