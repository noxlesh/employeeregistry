﻿
namespace EmployeeRegistry
{
    public class Position
    {
        public Position()
        {

        }

        public Position(string name)
        {
            Name = name;
        }
        public const int MaxSize = 96;
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
